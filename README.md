# Sample web server configuration

Целью данного *README.md* является знакомство с форматом Markdown. **Настоятельно рекомендуется воспринимать все, что написано ниже как полет воспаленной фантазии.**

Merge requests to this project are welcome!

You can start by reading the [tutorial](doc/index.html).

## Index

* [Prerequisites](#prerequisites)
* [Features](#features)
* [Running](#running)
* [Database](#database)
* [Tests](#tests)

## Screenshot

Так отвечает свежеустановленный nginx

![izobrazhenie_2020-12-22_195754](doc/izobrazhenie_2020-12-22_195754.png)

## Prerequisites

1. [MariaDB][https://mariadb.org] 10.2.0 or above installed.
2. php 7.4

## Features

* Fast working
* Load balancing
* Profile page with ability to update the password and remove the account
* Can use PostgreSQL database instead MariaDB/MySQL


### TODO

* add load balancing features
* add forget password feature
* add email confirmation

## Running

After start the database:

    sudo mysql_secure_installation

To start a web server for the application, run:

    sudo systemctl start nginx

Now visit http://localhost:80/ to see the nginx running.

### Run as container

Just download `Dockerfile` from Dockerhub. You can build and run the app with Docker.
App will be running on port `80` by default. Database is not included.
So make sure to pass a `DATABASE_URL` to the container.


## Database

Application reads the `DATABASE_URL` environment variable for the database connection.


## Tests

To run tests you need to place this code in index.php file
```
<!DOCTYPE html>
<html>
  <head>
      <title>Пример</title>
  </head>
  <body>

      <?php
      echo "Привет, я - скрипт PHP!";
      ?>

  </body>
</html>
```

    # run tests
    curl http://localhost:80/index.php
 
